package com.onlinestore.onlinestore.controller;

import com.onlinestore.onlinestore.dto.LoginDto;
import com.onlinestore.onlinestore.dto.LoginResponseDto;
import com.onlinestore.onlinestore.dto.RegistrationDto;
import com.onlinestore.onlinestore.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;


@Log4j2
@Controller
@RequiredArgsConstructor
@RequestMapping("/signin")
public class LoginController {

    private final UserService userService;

    @GetMapping
    public String getLogin() {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (!(auth instanceof AnonymousAuthenticationToken)) {
//            return "redirect:/dashboard";
//        }
        return "signin";
    }


    @PostMapping
    public RedirectView login(LoginDto form) {
        userService.login(form);
        return new RedirectView("/dashboard");
    }

}




