package com.onlinestore.onlinestore.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
public class JwtService {

    private Key key;

    @Value("${token.duration}")
    private Long duration;


    public void init() {
        byte[] keyBytes;
        keyBytes= Decoders.BASE64.decode(
                "dGhpcyBpcyBteSBzZWNyZXQga2V5IGZvciBqd3QgYmFzZTY0IGJhc2U2NC4gYmFzZTY0IGJhc2U2NCBmb3JoYW5keVN0b3Jl");
                key= Keys.hmacShaKeyFor(keyBytes);

    }
    public Claims parseToken(String token){

        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(JwtCredentials jwtCredentials) {
        log.trace("Issue JWT token to {} for {} seconds", jwtCredentials, duration);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setIssuedAt(new Date())
                .claim(JwtCredentials.Fields.email, jwtCredentials.getEmail())
                .claim(JwtCredentials.Fields.id, jwtCredentials.getId())
                .claim(JwtCredentials.Fields.role, jwtCredentials.getRole())
                .setExpiration(Date.from(Instant.now().plusSeconds(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }


}
