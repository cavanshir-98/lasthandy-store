package com.onlinestore.onlinestore.security.jwt;


import com.onlinestore.onlinestore.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Data
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtCredentials {

    String email;
    List<Role> role;
    Long id;
}
