package com.onlinestore.onlinestore.security;

import com.onlinestore.onlinestore.security.jwt.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilterConfigurerAdapter jwtFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors().disable()
                .authorizeRequests()
                .antMatchers("/signup", "/index", "/resetpass/**", "/img/**", "/css/**", "/js/**", "/uploaded/**")
                .permitAll()
                .antMatchers(
                        "/dashboard", "/message",
                        "/mypost", "/myposts", "/search",
                        "/info", "/update", "/user", "/wishlist").hasRole("USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/signin").permitAll()
                .defaultSuccessUrl("/info", true)
                .usernameParameter("login")
                .passwordParameter("pass")
                .and()
                .logout()
                .logoutUrl("/logout")
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/");
        http.apply(jwtFilter);


//        super.configure(http);
    }

}
